$(document).ready(function(){
    showPriceLists();
});

function showPriceLists() {
    $.ajax({
        url: base_url + '/admin/pricelists/get',
        dataType: 'json',
        success: function(data) {
            var pricelists = '';

            if (data.length == 0) {
                pricelists += '<tr>' +
                              '<td colspan="2" class="text-center">no price lists yet</td>'+
                              '</tr>';
            } else {
                
                for(var i = 0; i < data.length; i++) {

                    pricelists += '<tr>' +
                              '<td width="70%">' + data[i].name + '</td>' +
                              '<td width="30%" class="text-center">'+
                                '<div class="btn-group">' +
                                '<a href="' + base_url + '/admin/pricelists/products/' + data[i].id  + '/' + data[i].name + '" role="button" class="btn btn-outline-primary btn-sm hover-white">Products</a>' +
                                '<button role="button" class="btn btn-outline-info btn-sm edit_button" data-price_id="'+ data[i].id +'">Edit</button>' +
                                '<button role="button" class="btn btn-outline-danger btn-sm delete_button" data-price_id="'+ data[i].id +'">Delete</button>' +
                                '</div>' +
                              '</td>' +
                              '</tr>';

                }

            }

            $('#priceListsData').html(pricelists);
        }
    });
}

$('#addPriceListForm').on('submit', function(e) {
    e.preventDefault();
    
    $('#loading').show();

    $.ajax({
        url: base_url + '/admin/pricelists/add',
        type: 'POST',
        data: $(this).serialize(),
        success: function() {
            $('#loading').hide();
            $('#addPriceListModal').modal('hide');
            showPriceLists();
        }
    });
});

$('#priceListsData').on('click', '.edit_button', function() {
    var price_id = $(this).data('price_id');
    $('#updatePriceListModal').modal('show');

    $.ajax({
        url: base_url + '/admin/pricelists/get-price',
        type: 'POST',
        data: {id: price_id},
        dataType: 'json',
        success: function(data) {
            $('#price_name').val(data[0].name);
            $('#price_description').val(data[0].description);
        }
    });

});

$('#priceListsData').on('click', '.delete_button', function(){
    var price_id = $(this).data('price_id');
    $('#del_price_id').val(price_id);

    $('#deletePriceListsModal').modal('show');
});

$('#deletePriceForm').on('submit', function(e){
    e.preventDefault();

    $('#loading').show();

    $.ajax({
        url: base_url + '/admin/pricelists/delete',
        type: 'POST',
        data: $(this).serialize(),
        success: function() {
            $('#loading').hide();
            $('#deletePriceListsModal').modal('hide');
            showPriceLists();
        }
    });
});