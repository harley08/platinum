$('document').ready(function() {
    showProducts();
});

$('#addCategoryForm').on('submit', function(e){
    e.preventDefault();
    $('#loading').show();
    $.ajax({
        url: base_url + '/admin/pricelists/product/add-category',
        type: 'POST',
        data: $(this).serialize(),
        success: function() {
            $('#loading').hide();
            $('#addCategoryModal').modal('hide');
            showProducts();
        }
    });    
});

function showProducts() {
    $.ajax({
        url: base_url + '/admin/pricelists/products/get-category',
        type: 'POST',
        data: {priceid: priceid},
        dataType: 'json',
        success: function(data) {
            var categories = '';

            if (data.length == 0) {
                categories += '<div class="card-body"><p class="text-center">no categories yet</p></div>';
            } else {

                for (var i = 0; i < data.length; i++) {
                    categories +=   '<div class="card">' +
                                        '<div class="card-header" id="category'+ data[i].id +'">' +
                                            '<h2 class="mb-0">' +
                                                '<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse'+ data[i].id +'" aria-expanded="true" aria-controls="collapse'+ data[i].id +'">' +
                                                    '<i class="fa fa-box-open"></i> ' +
                                                    data[i].category_name +
                                                '</button>' +
                                            '</h2>' +
                                        '</div>' +
                                        '<div id="collapse'+ data[i].id +'" class="collapse '+ collapseShowFirst(i) +'" aria-labelledby="category'+ data[i].id +'" data-parent="#categoriesAccor">' +
                                            '<ul class="list-group list-group-lush" style="border-radius: 0px !important; margin: 10px;">' +
                                                show_packages(data[i].id) +
                                                '<li class="list-group-item package-add text-center">' +
                                                '<div class="btn-group">' +
                                                    '<button class="btn btn-outline-primary btn-sm" onclick="add_package('+ data[i].id +')"><i class="fa fa-plus"></i> Add Package</button>' +
                                                    '<button class="btn btn-outline-success btn-sm edit_category package-edit" onclick="edit_category('+ data[i].id +', \'' + data[i].category_name + '\', \'' + data[i].description + '\')"><i class="fa fa-edit"></i> Edit</button>' +
                                                    '<button class="btn btn-outline-danger btn-sm del_category" onclick="delete_category('+ data[i].id +', \'' + data[i].category_name + '\')">Delete</button>' +
                                                '</div>' +
                                                '</li>' +
                                            '</ul>' +
                                        '</div>'
                                    '</div>';
                }

            }

            $('#categories').html(categories);
        }
    });
}

function collapseShowFirst(no) {
    if (no == 0) {
        return 'show';
    }
}

function edit_category(categ_id, categ_name, description) {
    $('#EditCategoryModal').modal('show');
    $('#edit_name').val(categ_name);
    $('#e_category_id').val(categ_id);
    $('#e_category_desc').val(description);
}

$('#editCategoryForm').on('submit', function(e){
    e.preventDefault();
    $('#loading').show();
    $.ajax({
        url: base_url + '/admin/pricelists/products/edit-category',
        type: 'POST',
        data: $(this).serialize(),
        success: function() {
            $('#loading').hide();
            $('#EditCategoryModal').modal('hide');
            showProducts();
        }
    });
});

function add_package(package_id) {
    $('#addPackageModal').modal('show');
    $('#add_package_id').val(package_id);
}

$('#addPackageForm').on('submit', function(e){
    e.preventDefault();
    $('#loading').show();
    $.ajax({
        url: base_url + '/admin/pricelists/products/add-package',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function() {
            $('#loading').hide();
            $('#addPackageModal').modal('hide');
            showProducts();
            $('#category2').collapse();
        }
    });
});

function show_packages(package_id) {
        var result = '';
        $.ajax({
        url: base_url + '/admin/pricelists/products/get-package',
        type: 'POST',
        data: {package_id: package_id},
        async: false,
        dataType: 'json',
        success: function(data) {
            var packages = '';
            if (data.length == 0) {
                packages += '<li class="list-group-item package-add text-center">no packages yet</li>';
            } else {
                for (var i = 0; i < data.length; i++) {
                    packages +=  '<li class="list-group-item package-add"><span class="mr-5">'+
                                        '<i class="fa fa-box"></i> ' +
                                         data[i].package_name + '</span>' +
                                        '<div class="float-right">' +
                                            '<button type="button" class="btn btn-outline-primary btn-sm mr-1">Products</button>' +
                                            '<button type="button" class="btn btn-outline-secondary btn-sm mr-1">Add-ons</button>' +
                                            '<button type="button" class="btn btn-outline-success btn-sm mr-1">Edit Package</button>' +
                                            '<button type="button" class="btn btn-outline-danger btn-sm" onclick="deletePackage('+ data[i].id +')">Delete</button>' +
                                        '</div>' +
                                    ' </li>';
                }
            }

            result = packages;
        }
        });
        return result;
}

function deletePackage(id) {
    $('#deletePackageModal').modal('show');
    $('#del_package_id').val(id);
}

$('#deletePackageForm').on('submit', function(e){
    e.preventDefault();
    $('#loading').show();
    $.ajax({
        url: base_url + '/admin/pricelists/products/del-package',
        type: 'POST',
        data: $(this).serialize(),
        success: function(){
            $('#loading').hide();
            $('#deletePackageModal').modal('hide');
            showProducts();
        }
    });
});

function delete_category(id, name) {
    $('#del_pricelist_id').val(id);
    $('#del_pricelist_name').text(name);
    $('#deleteCategoryModal').modal('show');
}

$('#deleteCategoryForm').on('submit', function(e){
    e.preventDefault();
    $('#loading').show();
    $.ajax({
        url: base_url + '/admin/pricelists/products/del-category',
        type: 'POST',
        data: $(this).serialize(),
        success: function(){
            $('#loading').hide();
            showProducts();
            $('#deleteCategoryModal').modal('hide');
        }
    });
});