
$('#addFolderForm').on('submit', function(e){
    
    e.preventDefault();

    $('#loading').show();

    $.ajax({
        url: base_url + '/admin/galleries/action/add-folder',
        type: 'POST',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        success: function() {

            $('#loading').hide();

            var x = document.getElementById("snackbar");
            x.className = "show";
            $('#snackbar_text').html('New Folder Successfully added!');
            setTimeout(function(){
                x.className = x.className.replace("show", "");
                window.location.href = base_url + '/admin/galleries';
            }, 3000);

        }
    });

});

$('#gallery_wc_img').on('change', function(){
    var file = this.files[0];
    var imageFile = file.type;
    var match = ["image/jpeg", "image/png", "image/jpg"];
    if (!((imageFile == match[0]) || (imageFile == match[1]) || (imageFile == match[2]))) {
        var notif = '<p>' +
                    '<small style="color: #ff0000"><b> * Please select a valid image file (JPEG/PNG/JPG). </b></small>' +
                    '</p>';
        $('#img_notif').html(notif);
    } else {
        $('#img_notif').html('');
    }
});