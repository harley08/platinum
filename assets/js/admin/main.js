$('document').ready(function(){
    show_galleries();
});

$('#logoutBtn').on('click', function(){
    var x = document.getElementById("snackbar");
    x.className = "show";
    $('#snackbar_text').html('Logging out...');
    setTimeout(function(){
        x.className = x.className.replace("show", "");
        window.location.href = base_url + '/admin/logout';
    }, 3000);
});

function show_galleries() {
    $.ajax({
        url: base_url + '/admin/galleries/groups/get',
        dataType: 'json',
        success: function(data) {
            var galleries = '';

            if (data.length == 0) {
                galleries = '<a class="collapse-item" href="#" disabled>no galleries yet</a>';
            } else {

                for(var i = 0; i < 4; i++) {
                    galleries += '<a class="collapse-item" href="#" disabled>'+ data[i].gname +'</a>';
                }

            }

            $('#galleries').html(galleries);

        }
    });
}