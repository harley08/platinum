var base_url = 'http://localhost/platinum';

$('#loginForm').submit(function(e){
    e.preventDefault();

    $.ajax({
        url: base_url + '/admin/get-account',
        type: 'POST',
        data: $(this).serialize(),
        dataType: 'json',
        success: function(data) {
            if(data['logged_info'] == 'error') {
                $('#alert').html('<div class="alert alert-warning alert-dismissible fade show" role="alert">Incorrect Username or Password <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            } else {
                $('#alert').html('<div class="alert alert-success alert-dismissible fade show" role="alert">Success! Please wait.  <img src="' + base_url + '/assets/loading/loading.gif" width="15px"></div>');
                window.setTimeout(function(){
                    window.location.href = base_url + '/admin/dashboard';
                }, 2000);
            }
        }
    });
});