$(document).ready(function(){
    showFolders();
});

function showFolders() {
    $.ajax({
        url: base_url + '/admin/galleries/folders/view-all',
        dataType: 'json',
        success: function(data) {

            var folders = '';

            if (data.length == 0) {
                folders += '<tr>'+
                            '<td colspan="2" class="text-center">no folders yet</td>' +
                            '</tr>';
            } else {
                
                for (var i = 0; i < data.length; i++) {
                    folders += '<tr>' +
                                '<td>' + data[i].folder_name + '</td>' +
                                '<td class="text-center">'+
                                    '<div class="btn-group">'+
                                    '<button class="btn btn-outline-primary"><i class="fa fa-cog"></i> Folder Control Panel</button>' +
                                    '<button type="button" class="btn btn-outline-danger delete_folder" data-folder_id="'+ data[i].id +'"><i class="fa fa-times"></i> Delete</button>' +
                                    '</div>' +
                                '</td>' +
                                '</tr>';
                }

            }

            $('#foldersData').html(folders);

        }
    });
}

$('#foldersData').on('click', '.delete_folder', function(){
    var folder_id = $(this).data('folder_id');
    
    $('#folder_id').val(folder_id);
    $('#deleteFolderModal').modal('show');
});

$('#deleteFolderForm').on('submit', function(e){
    e.preventDefault();

    $('#loading').show();

    $.ajax({
        url: base_url + '/admin/galleries/folder/delete-folder',
        type: 'POST',
        data: $(this).serialize(),
        success: function() {
            $('#loading').hide();
            showFolders();
            $('#deleteFolderModal').modal('hide');
        }
    });
});