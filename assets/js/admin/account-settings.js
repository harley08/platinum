$('document').ready(function(){
    get_admin_data();
});

function get_admin_data() {
    $.ajax({
        url: base_url + '/admin/get-account-data',
        dataType: 'json',
        success: function(data) {
            $('#fname').val(data['fname']);
            $('#lname').val(data['lname']);
            $('#uname').val(data['uname']);
            $('#upw').val('******************');
            $('#log_attemp').val(data['log_attemp']);
            $('#ia_code').val(data['access_code']);
            $('#company').val(data['company_name']);
        }
    });
}

$('#updateForm').on('submit', function(e){
    e.preventDefault();
    
    $.ajax({
        url: base_url + '/admin/update-account-data',
        type: 'POST',
        data: $(this).serialize(),
        success: function(){
            get_admin_data();

            var x = document.getElementById("snackbar");
            x.className = "show";
            $('#snackbar_text').html('Updated Successfully!');
            setTimeout(function(){
                x.className = x.className.replace("show", "");
            }, 3000);

        }
    });
});

$('#new_pw').attr('disabled', 'disabled');
$('#re_new_pw').attr('disabled', 'disabled');
$('#updateBtn').attr('disabled', 'disabled');

$('#curr_pw').on('keyup', function(){

    var curr_pw = $(this).val();

    $.ajax({
        url: base_url + '/admin/get-curr-password',
        type: 'POST',
        data: {curr_pw: curr_pw},
        dataType: 'json',
        success: function(data) {
            if(data['verified'] == '1') {
                $('#curr_pw_notif').html('<font color="green"><b><i class="fa fa-check"></i></b></font>');
                $('#new_pw').removeAttr('disabled');
            } else {
                $('#curr_pw_notif').html('<font color="red"><b><i class="fa fa-times"></i></b></font>');
            }
        }
    });
});

var WeakPass = /(?=.{5,}).*/;
var MediumPass = /^(?=\S*?[a-z])(?=\S*?[0-9])\S{5,}$/;
var StrongPass = /^(?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])\S{5,}$/;
var VryStrongPass = /^(?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9])(?=\S*?[^\w\*])\S{5,}$/;



$('#new_pw').on('keyup', function(){
    if(VryStrongPass.test($(this).val())) {
        $('#pw_notif').html('<font color="#64dd17"><b>Very Strong!</b></font>');
        $('#re_new_pw').removeAttr('disabled');
    }   
    else if(StrongPass.test($(this).val())) {
        $('#pw_notif').html('<font color="#aeea00"><b>Strong!</b></font>');
        $('#re_new_pw').removeAttr('disabled');
    }   
    else if(MediumPass.test($(this).val())) {
        $('#pw_notif').html('<font color="#ffd600"><b>Good!</b></font>');
        $('#re_new_pw').removeAttr('disabled');
    }
    else if(WeakPass.test($(this).val())) {
        $('#pw_notif').html('<font color="#ff1744"><b>Still Weak password!</b></font>');
        $('#re_new_pw').attr('disabled', 'disabled');
    }
    else {
        $('#pw_notif').html('<font color="#d50000"><b>Weak password!</b></font>');
        $('#re_new_pw').attr('disabled', 'disabled');
    }
});

$('#re_new_pw').on('keyup', function(){
    if($(this).val() == $('#new_pw').val()) {
        $('#updateBtn').removeAttr('disabled');
    } else {
        $('#updateBtn').attr('disabled', 'disabled');
    }
});

$('#updatePasswordForm').on('submit', function(e){
    e.preventDefault();

    $.ajax({
        url: base_url + '/admin/update-account-password',
        type: 'POST',
        data: $(this).serialize(),
        success: function(){

            $('#updatePasswordModal').modal('hide');

            var x = document.getElementById("snackbar");
            x.className = "show";
            $('#snackbar_text').html('Password Changed Successfully!');
            setTimeout(function(){
                x.className = x.className.replace("show", "");
            }, 3000);
        }
    });
});

$('.btn-generate').on('click', function(){
    var btn = $(this).button('loading');
    window.setTimeout(function(){
        btn.button('reset');

        $.ajax({
            url: base_url + '/admin/generate-new-access-code',
            success: function() {
                get_admin_data();
            }
        });

    }, 3000);
});