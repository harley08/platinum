<?php

defined('BASEPATH') OR exit('No direct script access allowed!');

class Folders extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_Folders', 'folder');
    }

    public function view() {
        $data = $this->folder->view();
        echo json_encode($data);
    }

    public function add() {

        $file_directory = 'assets/user/gallery-welcome-img/';
        $wc_img = $file_directory . 'camera.png';
        
        $config['allowed_types'] = 'gif|jpg|png';
        $config['upload_path'] = './assets/user/gallery-welcome-img';

        $this->load->library('upload', $config);

        if($this->upload->do_upload('gallery_wc_img')) {
            print_r($this->upload->data());
            $file_data = $this->upload->data();
            $wc_img = $file_directory . $file_data['file_name'];
        } else {
            print_r($this->upload->display_errors());
        }


        $data = array(
            'folder_name' => $this->input->post('folder_name'),
            'folder_password' => $this->input->post('folder_password'),
            'wc_img' => $wc_img,
            'folder_text' => $this->input->post('folder_text'),
            'homepage_text' => $this->input->post('homepage_text')
        );
        
        $this->folder->add($data);
    }

    public function delete() {
        $folder_id = $this->input->post('folder_id');
        $this->folder->delete($folder_id);
    }

}

?>