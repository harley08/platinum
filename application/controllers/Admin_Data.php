<?php

defined('BASEPATH') OR exit('No direct script access allowed!');

class Admin_Data extends CI_Controller {
    
    function __construct() {
        parent::__construct();

        $this->load->model('M_Admin_Account', 'admin');

        if(!$this->session->userdata('admin_logged')) {
            redirect('');
        }
    }

    public function admin_data() {
        $userid = $this->session->userdata('userid');
        $data = $this->admin->account_data($userid);
        echo json_encode($data);
    }

    public function update_account() {
        $userid = $this->session->userdata('userid');
        $this->admin->update_account($userid);
    }

    public function update_password() {
        $userid = $this->session->userdata('userid');
        $new_password = $this->input->post('new_pw');
        $this->admin->update_password($userid, $new_password);
    }

    public function get_curr_password() {
        $userid = $this->session->userdata('userid');
        $curr_pw = $this->input->post('curr_pw');

        if(count($this->admin->get_curr_password($userid, $curr_pw))) {
            $verfified = 1;
        } else {
            $verfified = 0;
        }

        $data = array('verified' => $verfified);
        echo json_encode($data);
    }

    public function generate_access_code() {
        $userid = $this->session->userdata('userid');

        function generate_code($length = 4) {
            return substr(str_shuffle(str_repeat($x='012345678901234567890123456789012345678901234567890123456789', ceil($length/strlen($x)) )),1,$length);
        }

        $access_code = generate_code(5,1,"lower_case,upper_case");
        $this->admin->generate_access_code($userid, $access_code);

    }
}

?>