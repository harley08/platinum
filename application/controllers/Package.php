<?php

defined('BASEPATH') OR exit('No direct script access allowed!');

class Package extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('M_Package', 'package');
    }

    public function add() {

        // Package Image Upload
        $config                     =   array();
        $config['upload_path']      =   './assets/user/products/package_imgs';
        $config['log_threshold']    =   1;
        $config['allowed_types']    =   'gif|png|jpeg|jpg';
        $config['max_size']         =   '100000';
        $config['file_name']        =   'package_img';
        $config['overwrite']        =      false;

        $this->load->library('upload', $config, 'package_img_upload');
        $this->package_img_upload->initialize($config);
        $upload_package_img =   $this->package_img_upload->do_upload('package_img');

        // Sample Image Upload
        $config                     =   array();
        $config['file_name']        =  'sample_img';
        $config['upload_path']      =  './assets/user/products/sample_imgs';
        $config['allowed_types']    =  'gif|png|jpeg|jpg';
        $config['max_size']         =   '100000';
        $config['overwrite']        =   false;

        $this->load->library('upload', $config, 'sample_img_upload');
        $this->sample_img_upload->initialize($config);
        $upload_sample_img  =   $this->sample_img_upload->do_upload('sample_img');

        if ($upload_package_img && $upload_sample_img) {

            $package_img_data   =   $this->package_img_upload->data();
            $package_img        =   $package_img_data['file_name'];

            $sample_img_data    =   $this->sample_img_upload->data();
            $sample_img         =   $sample_img_data['file_name'];

        } else {
            $package_img    =   'no-img.png';
            $sample_img     =   'no-img.png';
        }

        $this->package->add($package_img, $sample_img);
    }

    public function get() {
        $id = $this->input->post('package_id');
        $data = $this->package->get($id);
        echo json_encode($data);
    }

    public function delete() {
        $id = $this->input->post('package_id');
        $this->package->delete($id);
    }
}

?>