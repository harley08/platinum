<?php

defined('BASEPATH') OR exit('No direct script access allowed!');

class PriceLists extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_PriceLists', 'pricelists');
        $this->load->model('M_Admin_Account', 'admin');

        if(!$this->session->userdata('admin_logged')) {
            redirect('admin/login');
        }

    }

    public function products($price_id) {
        $price_data = $this->pricelists->get_price($price_id);

        $data['title'] = $price_data[0]->name . ' Products | Platinum Photography';

        $userid = $this->session->userdata('userid');
        $data['user'] = $this->admin->account_data($userid);
        $data['price_data'] = $price_data;

        $this->load->view('admin/layouts/header', $data);
        $this->load->view('admin/layouts/sidebar');
        $this->load->view('admin/layouts/topbar');
        $this->load->view('admin/products');
        $this->load->view('admin/layouts/footer');
    }

    public function get() {
        $data = $this->pricelists->get();
        echo json_encode($data);
    }

    public function add() {
        $this->pricelists->add();
    }

    public function get_price() {
        $id = $this->input->post('id');
        $data = $this->pricelists->get_price($id);
        
        echo json_encode($data);
    }

    public function delete() {
        $id = $this->input->post('id');
        $data = $this->pricelists->delete($id);
    }

}

?>