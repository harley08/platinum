<?php
class Home extends CI_Controller {
    public function index() {

        $data['title'] = 'Platinify';

        $this->load->view('layouts/header', $data);
        $this->load->view('layouts/navigation');
        $this->load->view('home/index');
        $this->load->view('layouts/footer');
    }
}
?>