<?php

defined('BASEPATH') OR exit('No direct script access allowed!');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();

        date_default_timezone_set('Asia/Manila');
        $this->load->model('M_Admin_Account', 'admin');
    }

    public function admin_login() {
        if ($this->session->userdata('admin_logged')) {
            redirect('admin/dashboard');
        }
        
        $data['title'] = 'Login | Platinum Photography';

        $this->load->view('admin/login/index', $data);
    }

    public function admin_logout() {
        $sess = array('admin_logged' => FALSE, 'userid' => '');
        $this->session->unset_userdata($sess);
        $this->session->sess_destroy();
        redirect('admin/login');
    }

    public function get_account() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $result = $this->admin->get_account($username, $password);

        if (count($result)) {
            $sess_data = array(
                'userid' => $result['id'],
                'admin_logged' => TRUE
            );

            $this->session->set_userdata($sess_data);
            $logged_info = 'success';
        } else {
            $logged_info = 'error';
        }

        $data = array('logged_info' => $logged_info);
        echo json_encode($data);


    }
    
}

?>