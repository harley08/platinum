<?php

defined('BASEPATH') OR exit('No direct script access allowed!');

class Admin extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('M_Admin_Account', 'admin');

        if(!$this->session->userdata('admin_logged')) {
            redirect('admin/login');
        }
    }

    public function index() {
        $data['title'] = 'Dashboard | Platinum Photography';

        $userid = $this->session->userdata('userid');
        $data['user'] = $this->admin->account_data($userid);

        $this->load->view('admin/layouts/header', $data);
        $this->load->view('admin/layouts/sidebar');
        $this->load->view('admin/layouts/topbar');
        $this->load->view('admin/index');
        $this->load->view('admin/layouts/footer');
    }

    public function galleries() {
        $data['title'] = 'Galleries | Platinum Photography';

        $userid = $this->session->userdata('userid');
        $data['user'] = $this->admin->account_data($userid);

        $this->load->view('admin/layouts/header', $data);
        $this->load->view('admin/layouts/sidebar');
        $this->load->view('admin/layouts/topbar');
        $this->load->view('admin/galleries');
        $this->load->view('admin/layouts/footer');
    }

    public function add_folder() {
        $data['title'] = 'Add Folder | Platinum Photography';

        $userid = $this->session->userdata('userid');
        $data['user'] = $this->admin->account_data($userid);

        $this->load->view('admin/layouts/header', $data);
        $this->load->view('admin/layouts/sidebar');
        $this->load->view('admin/layouts/topbar');
        $this->load->view('admin/add_folder');
        $this->load->view('admin/layouts/footer');
    }

    public function add_group() {
        $data['title'] = 'Add Group | Platinum Photography';

        $userid = $this->session->userdata('userid');
        $data['user'] = $this->admin->account_data($userid);

        $this->load->view('admin/layouts/header', $data);
        $this->load->view('admin/layouts/sidebar');
        $this->load->view('admin/layouts/topbar');
        $this->load->view('admin/add_group');
        $this->load->view('admin/layouts/footer');
    }

    public function pricelists() {
        $data['title'] = 'Price Lists | Platinum Photography';

        $userid = $this->session->userdata('userid');
        $data['user'] = $this->admin->account_data($userid);

        $this->load->view('admin/layouts/header', $data);
        $this->load->view('admin/layouts/sidebar');
        $this->load->view('admin/layouts/topbar');
        $this->load->view('admin/pricelists');
        $this->load->view('admin/layouts/footer');
    }

    public function import() {
        $data['title'] = 'Import | Platinum Photography';

        $userid = $this->session->userdata('userid');
        $data['user'] = $this->admin->account_data($userid);

        $this->load->view('admin/layouts/header', $data);
        $this->load->view('admin/layouts/sidebar');
        $this->load->view('admin/layouts/topbar');
        $this->load->view('admin/import');
        $this->load->view('admin/layouts/footer');
    }

    public function reports() {
        $data['title'] = 'Reports | Platinum Photography';

        $userid = $this->session->userdata('userid');
        $data['user'] = $this->admin->account_data($userid);

        $this->load->view('admin/layouts/header', $data);
        $this->load->view('admin/layouts/sidebar');
        $this->load->view('admin/layouts/topbar');
        $this->load->view('admin/reports');
        $this->load->view('admin/layouts/footer');
    }

    public function account_setting() {
        $data['title'] = 'Account Setting | Platinum Photography';

        $userid = $this->session->userdata('userid');
        $data['user'] = $this->admin->account_data($userid);

        $this->load->view('admin/layouts/header', $data);
        $this->load->view('admin/layouts/sidebar');
        $this->load->view('admin/layouts/topbar');
        $this->load->view('admin/account-setting');
        $this->load->view('admin/layouts/footer');
    }

}

?>