<?php

defined('BASEPATH') OR exit('No direct script access allowed!');

class Category extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_Category', 'category');
    }

    public function add() {
        $this->category->add();
    }

    public function get() {
        $priceid = $this->input->post('priceid');
        $data = $this->category->get($priceid);
        echo json_encode($data);
    }

    public function edit() {
        $this->category->edit();
    }

    public function delete() {
        $id = $this->input->post('id');
        $this->category->delete($id);
    }

}

?>