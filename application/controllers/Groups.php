<?php

defined('BASEPATH') OR exit('No direct script access allowed!');

class Groups extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('M_Groups', 'groups');
    }

    public function get() {
        $data = $this->groups->get();
        echo json_encode($data);
    }

    public function add() {
        
    }

}

?>