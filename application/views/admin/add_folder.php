
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo base_url('admin/galleries') ?>">Galleries</a>
                    </li>
                    <li class="breadcrumb-item active">Add Folder</li>
                </ol>
            </nav>

            <div class="card mb-4">
                <div class="card-header">
                    <h4 class="m-0 font-weight-bold text-primary">Folder Information</h4>
                </div>
                <div class="card-body">
                    <div class="alert alert-info"><b><font color="red">*</font></b> Indicates a required field</div>
                    <form enctype="multipart/form-data" id="addFolderForm">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Folder Name <b><font color="red">*</font></b></label> 
                                    <small>
                                        <a style="color: #2196f3" role="button" data-container="body" data-toggle="popover" data-placement="right" data-content="Select a name for this folder."><i class="fa fa-info-circle"></i></a>
                                    </small>
                                    <input type="text" name="folder_name" id="f_name" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Folder Password 
                                    <small>
                                        <a style="color: #2196f3" role="button" data-container="body" data-toggle="popover" data-placement="right" data-content="This password will allow customers to login and view all galleries in this folder."><i class="fa fa-info-circle"></i></a>
                                    </small>
                                    </label>
                                    <input type="password" name="folder_password" id="f_password" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Gallery Welcome Image 
                                    <small>
                                        <a style="color: #2196f3" role="button" data-container="body" data-toggle="popover" data-placement="right" data-content="Select an image to appear on the front page of every gallery in this Folder."><i class="fa fa-info-circle"></i></a>
                                    </small>
                                    </label>
                                    <span id="img_notif"></span>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="gallery_wc_img" id="gallery_wc_img">
                                        <label class="custom-file-label">Choose Image</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Folder Text
                                    <small>
                                        <a style="color: #2196f3" role="button" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter text to appear on the page that shows the list of links for galleries on this Folder."><i class="fa fa-info-circle"></i></a>
                                    </small>
                                    </label>
                                    <textarea name="folder_text" id="folder_text" cols="30" rows="10" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Gallery Homepage Text 
                                    <small>
                                        <a style="color: #2196f3" role="button" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter text to appear on the front page of every gallery in this Folder. Please note: This text will only appear on the front page of Galleries."><i class="fa fa-info-circle"></i></a>
                                    </small>
                                    </label>
                                    <textarea name="homepage_text" id="galler_hp_txt" cols="30" rows="10" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="btn-group">
                            <button type="submit" class="btn btn-outline-primary"
                                id="load"
                                data-loading-text="<i class='fa fa-spinner fa-spin'></i> Adding Folder...">
                            <small><i class="fa fa-plus"></i></small>
                            Add Folder</button>
                            <a href="<?php echo base_url('admin/galleries') ?>" class="btn btn-outline-danger">
                            <small><i class="fa fa-times"></i></small>
                            Cancel</a>
                        </div>
                    </form>
                </div>
            </div>

        </div>

        <script type="text/javascript">
            $(function () {
                $('[data-toggle="popover"]').popover();
            });

            ClassicEditor.create( document.querySelector( '#folder_text' ) )
                .catch( error => {
                    console.error( error );
                } );

                ClassicEditor.create( document.querySelector( '#galler_hp_txt' ) )
                .catch( error => {
                    console.error( error );
                } );
        </script>
        <script src="<?php echo base_url('assets/js/admin/add-folder.js') ?>"></script>