
        <div class="container-fluid">

            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Galleries</h1>
            </div>

            <ul class="nav nav-tabs" id="gallery" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="folder-tab" data-toggle="tab" href="#folder" role="tab" aria-controls="folder" aria-selected="true"><i class="fa fa-folder"></i> Folders</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="group-tab" data-toggle="tab" href="#group" role="tab" aria-controls="group" aria-selected="false"><i class="fa fa-folder"></i> Groups</a>
                </li>
            </ul>
            <div class="tab-content mb-4" id="gallery">
                <div class="tab-pane fade show active" id="folder" role="tabpanel" aria-labelledby="folder-tab">
                    <div class="card" style="border-top-left-radius: 0px !important; border-top-right-radius: 0px !important">
                        <div class="card-body">
                            <span class="float-left"><b><font color="red">*</font></b> Folders are an <b><i>optional</i></b> layer of organization for groups.</span>
                            <a  class="btn btn-outline-dark btn-sm float-right" href="<?php echo base_url('admin/galleries/add-folder') ?>"><i class="fa fa-folder-plus"></i> Add Folder</a>
                            <hr class="mt-5">

                            <div class="table-responsive">
                                <table class="table table-bordered" id="foldersTable" with="100%" cellspacing="0">
                                    <!-- <thead>
                                        <tr>
                                            <th>Folder Name</th>
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead> -->
                                    <tbody id="foldersData"></tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="group" role="tabpanel" aria-labelledby="group-tab">
                    <div class="card" style="border-top-left-radius: 0px !important; border-top-right-radius: 0px !important">
                        <div class="card-body">
                            <span class="float-left"><b><font color="red">*</font></b> Groups are a <b><i>required</i></b> layer of organization for galleries.</span>
                            <a class="btn btn-outline-dark btn-sm float-right" href="<?php echo base_url('admin/galleries/add-group') ?>"><i class="fa fa-folder-plus"></i> Add Group</a>
                            <hr class="mt-5">
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div id="deleteFolderModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><i class="fa fa-times"></i> Delete Folder</h5>
                        <button type="button" class="close" data-dismiss="modal">x</button>
                    </div>
                    <form id="deleteFolderForm">
                        <div class="modal-body text-center">
                            <b>Are you sure you want to remove this folder?</b>
                            <input type="hidden" name="folder_id" id="folder_id">
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-outline-primary">Yes</button>
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">No</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script src="<?php echo base_url('assets/js/admin/gallery.js') ?>"></script>