
        <div class="container-fluid">

            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Account Setting</h1>
            </div>

            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Update Information</h6>
                </div>
                <div class="card-body">
                    <form id="updateForm">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="firstname" id="fname" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" name="lastname" id="lname" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input type="text" name="username" id="uname" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Password</label>
                                    <div class="input-group">
                                        <input type="text" name="upw" id="upw" class="form-control" disabled>
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#updatePasswordModal">
                                                <small><i class="fa fa-lock"></i> Change Password</small>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Limit Login Attempts</label>
                                    <input type="number" name="login_attemp" id="log_attemp" class="form-control" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Access Code</label>
                                    <div class="input-group">
                                        <input type="text" name="access_code" id="ia_code" class="form-control" disabled>
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-outline-secondary btn-generate btn-sm" id="load" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Re-Generating...">
                                                <small><i class="fas fa-fw fa-sync"></i> Re-Generate</small>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" name="company_name" id="company" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-sm btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-edit"></i>
                        </span>
                        <span class="text">Update</span>
                        </button>
                    </form>
                </div>
            </div>

        </div>

        <div id="updatePasswordModal" class="modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update Password</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <form id="updatePasswordForm">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Current Password <small><span id="curr_pw_notif"></span></small></label>
                                <input type="password" name="curr_pw" id="curr_pw" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>New Password <small><span id="pw_notif"></span></small></label>
                                <input type="password" name="new_pw" id="new_pw" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Re-Type Password</label>
                                <input type="password" name="re_new_pw" id="re_new_pw" class="form-control">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-primary btn-sm" id="updateBtn">Update Password</button>
                                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script src="<?php echo base_url('assets/js/admin/account-settings.js') ?>"></script>