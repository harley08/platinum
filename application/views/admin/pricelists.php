
        <div class="container-fluid">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Price Lists</h1>
        </div>

        <div class="mb-4">
            <button type="button" data-toggle="modal" data-target="#addPriceListModal" class="btn btn-outline-primary btn-sm"><i class="fa fa-plus"></i> Add Price List</button>
        </div>

        <div class="card mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="priceListsTable" class="table table-bordered">
                        <thead>
                            <tr>
                                <th colspan="2">Price Lists Name</th>
                            </tr>
                        </thead>
                        <tbody id="priceListsData"></tbody>
                    </table>
                </div>
            </div>
        </div>

        </div>

        <div id="addPriceListModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Price</h5>
                        <button type="button" class="close" data-dismiss="modal">x</button>
                    </div>
                    <form id="addPriceListForm">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-outline-primary btn-sm">Add</button>
                                <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-sm">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="updatePriceListModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update Price</h5>
                        <button type="button" class="close" data-dismiss="modal">x</button>
                    </div>
                    <form id="addPriceListForm">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" id="price_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" id="price_description" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-outline-primary btn-sm">Update</button>
                                <button type="button" data-dismiss="modal" class="btn btn-outline-danger btn-sm">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="deletePriceListsModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Delete Price</h5>
                        <button type="button" class="close" data-dismiss="modal">x</button>
                    </div>
                    <form id="deletePriceForm">
                        <div class="modal-body">
                            <p class="text-center">Are you sure you want to delete this price?</p>
                            <input type="hidden" name="id" id="del_price_id">
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-outline-danger btn-sm">Yes</button>
                                <button type="button" class="btn btn-outline-primary btn-sm" data-dismiss="modal">No</button>
                            </div>
                        </div>
                    </form>                    
                </div>
            </div>
        </div>

        <script src="<?php echo base_url('assets/js/admin/pricelists.js') ?>"></script>