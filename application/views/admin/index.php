
        <div class="container-fluid">

            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            </div>

            <div class="row">

                <div class="col-lg-6 mb-4">

                    <div class="card shadow">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary"><i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i> Settings</h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <a href="">System Design</a>
                            </li>
                            <li class="list-group-item">
                                <a href="">Gallery & Prepay Settings</a>
                            </li>
                            <li class="list-group-item">
                                <a href="">Price Lists Settings</a>
                            </li>
                            <li class="list-group-item">
                                <a href="">Checkout Settings</a>
                            </li>
                            <li class="list-group-item">
                                <a href="">Image Watermark</a>
                            </li>
                            <li class="list-group-item">
                                <a href="">Integrations</a>
                            </li>
                        </ul>
                    </div>

                </div>

                <div class="col-lg-6 mb-4">

                    <div class="card shadow">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Galleries</h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <a href="">Add Gallery</a>
                            </li>
                            <li class="list-group-item">
                                <a href="">Add Prepay Event</a>
                            </li>
                            <li class="list-group-item">
                                <a href="">Manage Existing Galleries</a>
                            </li>
                            <li class="list-group-item">
                                <a href="">View Expiring Galleries</a>
                            </li>
                            <li class="list-group-item">
                                <a href="">Custom Import</a>
                            </li>
                            <li class="list-group-item">
                                <a href="">Refresh Storefront Login Page</a>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>