
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<?php echo base_url('admin/pricelists') ?>">Price Lists</a>
                    </li>
                    <li class="breadcrumb-item active">Lists</li>
                </ol>
            </nav>

            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Products (<?php echo $price_data[0]->name ?>)</h1>
            </div>

            <div class="mb-4">
                <button type="button" class="btn btn-outline-primary btn-sm mr-2" data-toggle="modal" data-target="#addCategoryModal">Add Price List</button>
            </div>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="accordion" id="categoriesAccor">
                        <div id="categories"></div>
                    </div>
                </div>
            </div>
            
        </div>

        <div id="addCategoryModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Price List</h5>
                        <button type="button" class="close" data-dismiss="modal">x</button>
                    </div>
                    <form id="addCategoryForm">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Price List Name</label>
                                <input type="text" name="name" class="form-control">
                                <input type="hidden" name="userid" value="<?php echo $user['id'] ?>">
                                <input type="hidden" name="priceid" value="<?php echo $price_data[0]->id ?>">
                            </div>
                            <div class="form-group">
                                <label>Price List Description</label>
                                <textarea name="description" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-outline-primary btn-sm">Add</button>
                                <button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="EditCategoryModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Category</h5>
                        <button type="button" class="close" data-dismiss="modal">x</button>
                    </div>
                    <form id="editCategoryForm">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Category Name</label>
                                <input type="text" name="name" id="edit_name" class="form-control">
                                <input type="hidden" name="category_id" id="e_category_id">
                            </div>
                            <div class="form-group">
                                <label>Category Description</label>
                                <textarea name="description" class="form-control" id="e_category_desc"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-outline-primary btn-sm">Update</button>
                                <button type="button" class="btn btn-outline-danger btn-sm" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="addPackageModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Package</h5>
                    </div>
                    <form enctype="multipart/form-data" id="addPackageForm">
                        <input type="hidden" name="package_id" id="add_package_id">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Package Name <b><font color="red">*</font></b></label>
                                <input type="text" class="form-control" name="package_name" required>
                            </div>
                            <div class="form-group">
                                <label>Package Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="package_img" id="package_img">
                                    <label class="custom-file-label">Choose Image</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <input type="number" class="form-control" name="price">
                            </div>
                            <div class="form-group">
                                <label>Package Description</label>
                                <textarea name="package_description" id="package_description" cols="30" rows="10"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Sample Image</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="sample_img" id="sample_img">
                                    <label class="custom-file-label">Choose Image</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Sample Description</label>
                                <textarea name="sample_description" id="sample_description" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-outline-primary btn-sm">Add</button>
                                <button type="submit" class="btn btn-outline-danger btn-sm" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="deletePackageModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Delete Package</h5>
                    </div>
                    <form id="deletePackageForm">
                        <div class="modal-body">
                            <input type="hidden" name="package_id" id="del_package_id">
                            <p class="text-center">Are you sure you want to delete this package?</p>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-outline-primary">Yes</button>
                                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">No</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="deleteCategoryModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Delete Price List</h5>
                    </div>
                    <form id="deleteCategoryForm">
                        <div class="modal-body">
                            <input type="hidden" name="id" id="del_pricelist_id">
                            <p class="text-center">Are you sure you want to delete this price lists? <br> <b>"<span id="del_pricelist_name"></span>"</b></p>
                        </div>
                        <div class="modal-footer">
                            <div class="btn-group">
                                <button type="submit" class="btn btn-outline-success">Yes</button>
                                <button type="button" data-dismiss="modal" class="btn btn-outline-danger">No</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script>
            ClassicEditor.create( document.querySelector( '#package_description' ) )
            .catch( error => {
                console.error( error );
            } );
            ClassicEditor.create( document.querySelector( '#sample_description' ) )
            .catch( error => {
                console.error( error );
            } );

        </script>
        <script>var priceid = <?php echo $price_data[0]->id ?></script>
        <script src="<?php echo base_url('assets/js/admin/products.js') ?>"></script>