<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Arrow Up Media, Co. Ltd.">
        <link rel="icon" href="<?php echo base_url('assets/images/icon/icon.png') ?>">

        <title><?php echo $title ?></title>

        <link rel="stylesheet" href="<?php echo base_url('assets/sb-admin/vendor/fontawesome-free/css/all.min.css') ?>">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="<?php echo base_url('assets/sb-admin/css/sb-admin-2.min.css') ?>" rel="stylesheet">
        <link href="<?php echo base_url('assets/custom/css/admin-styles.css') ?>" rel="stylesheet">

        <script src="<?php echo base_url('assets/sb-admin/vendor/jquery/jquery.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/sb-admin/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/sb-admin/vendor/jquery-easing/jquery.easing.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/ckeditor/ckeditor.js') ?>"></script>

    </head>
    <body id="page-top">

        <!-- Loading -->
        <div id="loading" style="display: none">
            <div class="spinner">
                <h3>Loading</h3>
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>

        <div id="wrapper">
        