        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url('admin/dashboard') ?>">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-camera-retro"></i>
                </div>
                <div class="sidebar-brand-text mx-3">PLATINUM</div>
            </a>

            <hr class="sidebar-divider my-0">

            <li class="nav-item active">
                <a class="nav-link" href="<?php echo base_url('admin/dashboard') ?>">
                    <i class="fas fa-fw fa-tachometer-alt"></i> 
                    <span>Dashboard</span>    
                </a>
            </li>

            <hr class="sidebar-divider">

            <div class="sidebar-heading">
                MENUS
            </div>

            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url() ?>">
                    <i class="fas fa-fw fa-globe"></i>
                    <span>View Store</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#otherMenus" aria-expanded="true" aria-controls="otherMenus">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Galleries</span>
                </a>
                <div id="otherMenus" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Lists:</h6>
                        <div id="galleries"></div>
                        <a class="collapse-item" href="<?php echo base_url('admin/galleries') ?>"><b>VIEW ALL</b></a>
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('admin/price-lists') ?>">
                    <i class="fas fa-fw fa-dollar-sign"></i>
                    <span>Price Lists</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('admin/import') ?>">
                    <i class="fas fa-fw fa-file-import"></i>
                    <span>Import</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('admin/reports') ?>">
                    <i class="fas fa-fw fa-receipt"></i>
                    <span>Reports</span>
                </a>
            </li>

            <hr class="sidebar-divider d-none d-md-block">

            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>

        <div id="content-wrapper" class"d-flex flex-column">
            <div id="main-content">
            
            