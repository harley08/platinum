
                </div>

                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Platinum Photography 2019</span>
                        </div>
                    </div>
                </footer>

            </div>

        </div>

        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to logout?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" id="logoutBtn" style="color: #fff" data-dismiss="modal">Logout</a>
                    </div>
                </div>
            </div> 
        </div>

        <div id="snackbar"><span id="snackbar_text"></span></div>

        <script>var base_url = 'http://localhost/platinum';</script>
        <script src="<?php echo base_url('assets/jquery/button.js') ?>"></script>
        <script src="<?php echo base_url('assets/sb-admin/js/sb-admin-2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/admin/main.js') ?>"></script>
    </body>
</html>