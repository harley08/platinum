        <nav class="navbar navbar-expand-md navbar-dark bg-platinum fixed-top">

            <div class="container">
                <a class="navbar-brand" href="">
                    <img src="<?php echo base_url('assets/images/logo/logo.png') ?>" alt="Platinify" width="90px">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbars">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="">Platinum Photography</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href=""><i class="fa fa-shopping-cart"></i> Cart</a>
                        </li>
                    </ul>
                </div>
            </div>

        </nav>