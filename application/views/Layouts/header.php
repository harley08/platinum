<!DOCTYPE HTML>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="Arrow Up Media">
        <link rel="icon" href="<?php echo base_url('assets/images/icon/icon.png') ?>">
        <title><?php echo $title ?></title>

        <!-- BOOTSTRAP CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">

        <!-- CUSTOM CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/custom/css/styles.css') ?>">

        <!-- FONT AWESOME -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        <!-- JQUERY -->
        <script src="<?php echo base_url('assets/jquery/jquery-2.2.3.min.js') ?>"></script>

        <!-- BOOTSTRAP JS -->
        <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>

    </head>
    <body>
    