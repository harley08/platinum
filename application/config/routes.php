<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['admin/galleries/groups/get'] = 'Groups/get';

$route['admin/pricelists/products/del-package'] = 'Package/delete';
$route['admin/pricelists/products/get-package'] = 'Package/get';
$route['admin/pricelists/products/add-package'] = 'Package/add';

$route['admin/pricelists/products/del-category'] = 'Category/delete';
$route['admin/pricelists/products/edit-category'] = 'Category/edit';
$route['admin/pricelists/products/get-category'] = 'Category/get';
$route['admin/pricelists/product/add-category'] = 'Category/add';

$route['admin/pricelists/products/(:num)/(:any)'] = 'PriceLists/products/$1/$1';

$route['admin/pricelists/delete'] = 'PriceLists/delete';
$route['admin/pricelists/get-price'] = 'Pricelists/get_price';
$route['admin/pricelists/add'] = 'PriceLists/add';
$route['admin/pricelists/get'] = 'PriceLists/get';

$route['admin/galleries/folder/delete-folder'] = 'Folders/delete';
$route['admin/galleries/folders/view-all'] = 'Folders/view';
$route['admin/galleries/action/add-folder'] = 'Folders/add';

$route['admin/generate-new-access-code'] = 'Admin_data/generate_access_code';
$route['admin/update-account-password'] = 'Admin_data/update_password';
$route['admin/get-curr-password'] = 'Admin_data/get_curr_password';
$route['admin/update-account-data'] = 'Admin_data/update_account';
$route['admin/get-account-data'] = 'Admin_data/admin_data';

$route['admin/logout'] = 'User/admin_logout';
$route['admin/get-account'] = 'User/get_account';

$route['admin/galleries/add-group'] = 'Admin/add_group';
$route['admin/galleries/add-folder'] = 'Admin/add_folder';

$route['admin/login'] = 'User/admin_login';
$route['admin/account-setting'] = 'Admin/account_setting';
$route['admin/reports'] = 'Admin/reports';
$route['admin/import'] = 'Admin/import';
$route['admin/price-lists'] = 'Admin/pricelists';
$route['admin/galleries'] = 'Admin/galleries';
$route['admin/dashboard'] = 'Admin';

$route['default_controller'] = 'Home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;