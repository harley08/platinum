<?php

if(!defined('BASEPATH')) exit('No direct script access allowed!');

class M_Admin_Account extends CI_Model {
    
    function __construct() {
        parent::__construct();
    }

    public function get_account($username, $password) {
        $key = $this->config->item('encryption_key');
        $salt3 = '!@#GH^%)-:HDF%';
        $salt1 = hash('sha512', $key . $password  . $salt3);
        $salt2 = hash('sha512', $password . $key . $salt3);
        $hashed_password = hash('sha512', $salt1 . $password . $salt2 . $salt3);
        $password = $hashed_password;
            
        $data = array();
        $details = array('uname' => $username, 'upw' => $password);
        $this->db->where($details);
        $this->db->limit(1);
        $q = $this->db->get('system_admin');

        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }

        $q->free_result();
        return $data;
    }

    public function account_data($userid) {
        $data = array();
        $this->db->where('id', $userid);
        $q = $this->db->get('system_admin');

        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }

        $q->free_result();
        return $data;
    }

    public function update_account($userid) {
        $update = array(
            'fname' => $this->input->post('firstname'),
            'lname' => $this->input->post('lastname'),
            'uname' => $this->input->post('username'),
            'log_attemp' => $this->input->post('login_attemp'),
            'company_name' => $this->input->post('company_name')
        );

        $this->db->where('id', $userid);
        return $this->db->update('system_admin', $update);

    }

    public function get_curr_password($userid, $curr_pw) {
        $key = $this->config->item('encryption_key');
        $salt3 = '!@#GH^%)-:HDF%';
        $salt1 = hash('sha512', $key . $curr_pw  . $salt3);
        $salt2 = hash('sha512', $curr_pw . $key . $salt3);
        $hashed_password = hash('sha512', $salt1 . $curr_pw . $salt2 . $salt3);
        $curr_pw = $hashed_password;

        $criteria = array(
            'id' => $userid,
            'upw' => $curr_pw
        );

        $data = array();
        $this->db->where($criteria);
        $this->db->limit(1);
        $q = $this->db->get('system_admin');

        if ($q->num_rows() > 0) {
            $data = $q->row_array();
        }

        $q->free_result();
        return $data;
    }

    public function update_password($userid, $new_password) {
        $key = $this->config->item('encryption_key');
        $salt3 = '!@#GH^%)-:HDF%';
        $salt1 = hash('sha512', $key . $new_password  . $salt3);
        $salt2 = hash('sha512', $new_password . $key . $salt3);
        $hashed_password = hash('sha512', $salt1 . $new_password . $salt2 . $salt3);
        $new_password = $hashed_password;

        $update = array('upw' => $new_password);
    
        $this->db->where('id', $userid);
        return $this->db->update('system_admin', $update);

    }

    public function generate_access_code($userid, $access_code) {
        $this->db->where('id', $userid);
        $update = array('access_code' => $access_code);
        return $this->db->update('system_admin', $update);
    }

}

?>