<?php

if(!defined('BASEPATH')) exit('No direct script access allowed!');

class M_PriceLists extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get() {
        $data = $this->db->get('price_lists');
        return $data->result();
    }

    public function add() {

        $data = array(
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description')
        );

        return $this->db->insert('price_lists', $data);

    }

    public function get_price($id) {
        $this->db->where('id', $id);
        $data = $this->db->get('price_lists');

        return $data->result();
    }

    public function delete($id) {
        $this->db->where('id', $id);
        return $this->db->delete('price_lists');
    }

}

?>