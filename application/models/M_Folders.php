<?php

class M_Folders extends CI_Model {
    
    function __construct() {
        parent::__construct();
       
    }

    public function add($data) {
        return $this->db->insert('folders', $data);
    }

    public function view() {
        $data = $this->db->get('folders');
        return $data->result();
    }

    public function delete($folder_id) {
        $this->db->where('id', $folder_id);
        return $this->db->delete('folders');
    }

}

?>