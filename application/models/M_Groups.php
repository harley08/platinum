<?php

if(!defined('BASEPATH')) exit('No direct script access allowed!');

class M_Groups extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get() {
        $data = $this->db->get('groups');
        return $data->result();
    }

    public function add($wc_img) {
        $data = array(
            'gname' => $this->input->post('group_name'),
            'gpassword' => $this->input->post('group_password'),
            'expiration_date' => $this->input->post('expiration_date'),
            'wc_img' => $this->input->post('wc_img'),
            'folder_text' => $this->input->post('folder_text'),
            'homepage_text' => $this->input->post('homepage_text')
        );
    }

}

?>