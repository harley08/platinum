<?php

if(!defined('BASEPATH')) exit('No direct script access allowed!');

class M_Category extends CI_Model {

    function __construct() {
        parent:: __construct();
    }

    public function add() {
        $data = array(
            'user_id' => $this->input->post('userid'),
            'price_id' => $this->input->post('priceid'),
            'category_name' => $this->input->post('name'),
            'description' => $this->input->post('description')
        );

        return $this->db->insert('categories', $data);
    }

    public function get($priceid) {
        $this->db->where('price_id', $priceid);
        $data = $this->db->get('categories');
        return $data->result();
    }

    public function edit() {
        $id = $this->input->post('category_id');
        $name = $this->input->post('name');

        return $this->db->query("UPDATE categories SET category_name = '$name' WHERE id = $id");
    }

    public function delete($id) {
        $this->db->where('id', $id);
        return $this->db->delete('categories');
    }

}

?>