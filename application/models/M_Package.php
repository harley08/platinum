<?php

if(!defined('BASEPATH')) exit('No direct script access allowed!');

class M_Package extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    public function add($package_img, $sample_img) {
        $data = array(
            'package_id' => $this->input->post('package_id'),
            'package_name' => $this->input->post('package_name'),
            'package_img' => $package_img,
            'package_price' => $this->input->post('price'),
            'package_description' => $this->input->post('package_description'),
            'sample_img' => $sample_img,
            'sample_description' => $this->input->post('sample_description')
        );

        return $this->db->insert('packages', $data);
    }

    public function get($id) {
        $this->db->where('package_id', $id);
        $data = $this->db->get('packages');
        return $data->result();
    }

    public function delete($id) {
        $this->db->where('id', $id);
        return $this->db->delete('packages');
    }
}

?>